import http from "k6/http";
import { check, sleep } from "k6";

export let options = {
  // Количество пользователей одновременно зашедших на сайт
  vus: 75,
    // Время тестирования
  duration: "75s",
};

// Функция для отправки HTTP-запросов
export default function() {
  // Отправка GET-запроса на сайт
  const url = 'https://example.com/';
  const response = http.get(url);

// Проверка статусного кода ответа
    if (response.status !== 200 && response.status !== 0) {
        console.error(`Ошибка: Получен статусный код ${response.status} при запросе на ${url}`);
    }

 // Проверка наличия кода ошибок 400 – 499 и 500 – 599
 check(response, {
    'Status is 4xx or 5xx': (r) => {
      if (r.status >= 400 && r.status < 600)
      return r.status >= 400 && r.status < 600;
    }
  });

  // Проверка наличия кода ошибки 503
  check(response, {
    "Status is 503": (r) => r.status === 503
  });


  // Интервал между запросами (1 секунда)
  sleep(1);
}
